import React from 'react'

import PeopleMap from '../components/PeopleMap'
import { withGoogleAPI } from '../components/Map'

import db from '../db'

const stateIcons = {
  ok: require('../assets/icon-ok.png'),
  warn: require('../assets/icon-warn.png'),
  sos: require('../assets/icon-sos.png'),
}

export default withGoogleAPI(({ google }) => {
  const [lat, setLat] = React.useState(undefined)
  const [lng, setLng] = React.useState(undefined)
  const [markers, setMarkers] = React.useState([])

  React.useEffect(() => {
    function getUsers() {
      db.collection('users').get().then(snapshot => {
        const markerList = []
        snapshot.docs.map(doc => {
          const newLat = doc.get('lat')
          const newLng = doc.get('lng')
          setLat(newLat)
          setLng(newLng)
          const icon = {
            url: stateIcons[doc.get('state')],
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25),
          }
          
          const marker = {
            id: doc.get('name'),
            icon,
            title: 'lider',
            position: {
              lat: newLat,
              lng: newLng,
            },
          }
          markerList.push(marker)
        })
        setMarkers(markerList)
        setTimeout(() => {
          getUsers()
        }, 2000)
      }).catch(() => {
        setTimeout(() => {
          getUsers()
        }, 2000)
      })
    }

    getUsers()

  }, [])

  return (
    <div style={{ position: 'relative' }}>
      <div style={{ width: '100%', height: 600, flexShrink: 0, position: 'relative' }}>
        <PeopleMap
          markers={markers}
          polylines={[]}
          activeMarker={undefined}
          // showingInfoWindow={this.state.showingInfoWindow}
          // selectedPlace={this.state.selectedPlace}
          // onMapInstance={this.onMapInstance}
          // onMarkerClick={this.onMarkerClick}
          mapHeight={600}
        />
      </div>
      <img
        style={{
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: 150,
          height: 'auto',
        }}
        src={require('../assets/map-leyend.png')}
        alt="leyenda del mapa"
      />
    </div>
  )
})
