import * as firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyD21M93-JHfXBywAQr51hakBG3s49-dANo",
  authDomain: "protegematic.firebaseapp.com",
  databaseURL: "https://protegematic.firebaseio.com",
  projectId: "protegematic",
  storageBucket: "protegematic.appspot.com",
  messagingSenderId: "705431990403",
  appId: "1:705431990403:web:e4b276db8043dae0"
}

firebase.initializeApp(firebaseConfig)
