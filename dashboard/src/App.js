import React from 'react';
import { Switch, Route, Redirect, HashRouter as Router, NavLink } from 'react-router-dom'
import styled from '@emotion/styled'

import './App.css';

import MapSection from './MapSection'
import Login from './Login'
import Registro from './Registro'

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" component={MapSection} exact />
          <Route path="/informes" component={Reports}/>
          <Route path="/noticias" component={Soon}/>
          <Route path="/solicitudes" component={Soon}/>
          <Route path="/denuncias" component={Soon}/>
          <Route path="/streaming" component={Streaming}/>
          <Redirect path="/"/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;


const Header = () => (
  <HeaderContainer>
    <div style={{
      display: 'flex',
      alignItems: 'center',
      backgroundColor: 'black',
      height: 150,
    }}>
      <img src={require('./assets/logo-contrast.png')} alt="Lider Live logo" />
      <FutureSelect style={{ marginLeft: 200 }}>REGIÓN</FutureSelect>
      <FutureSelect style={{ marginLeft: 50 }}>NOMBRE</FutureSelect>
      <dir style={{
        marginRight: 20,
        marginLeft: 'auto',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
      }}>
        <img src={require('./assets/usaid.png')} alt="USAID logo" />
        <img src={require('./assets/ibm.png')} alt="IBM logo" />
      </dir>
    </div>
    <div style={{
      height: 50,
      display: 'flex',
      backgroundColor: 'black',
    }}>
      <StyledLink to='/' exact>MAPA</StyledLink>
      <StyledLink to='/informes'>INFORMES</StyledLink>
      <StyledLink to='/noticias'>NOTICIAS</StyledLink>
      <StyledLink to='/solicitudes'>SOLICITUDES</StyledLink>
      <StyledLink to='/denuncias'>DENUNCIAS</StyledLink>
      <StyledLink to='/streaming'>STREAMING</StyledLink>
    </div>
  </HeaderContainer>
) 

const HeaderContainer = styled.div({
  height: 200,
  backgroundColor: 'transparent',
})

const Soon = () => (
  <div style={{ margin: 20 }}>Próximamente ...</div>
)

const Reports = () => (
  <div style={{ position: 'relative', width: '100%', height: 600 }}>
    <iframe
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        border: 0,
      }}
     width="1280"
     height="600"
     src="https://app.powerbi.com/view?r=eyJrIjoiZGIxYTQ5ODAtMjdkYi00ZmY1LWE0NjUtOTBiMjZlOTcxZTI0IiwidCI6ImU4MTZlMmNmLTAwMDUtNDliYS1iNWI4LWVhNGRmM2JjZDY0NCJ9"
     frameborder="0"
     allowFullScreen="true"
    ></iframe>
  </div>
)

const Streaming = () => (
  <div>
    <div style={{
      margin: 20,
    }}>
      Lider anonimo
      Estado: S.O.S
      <br />
      <br />
      <Button>Alertar Autoridades</Button>
    </div>
    <div style={{
      margin: 20,
    }}>
      Ver situación en tiempo real:
    </div>
    <div style={{ marginLeft: 20, position: 'relative', width: '100%', height: 400 }}>
      <iframe
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          border: 0,
        }}
      width="1280"
      height="400"
      src="http://35.196.42.160/client2/examples/demo/streaming/player/player2.html?ruta=5070039"
      frameborder="0"
      allowFullScreen="true"
      ></iframe>
    </div>
  </div>
)

const StyledLink = styled(props => <NavLink activeStyle={{
  color: 'white',
  border: '2px solid white',
  borderBottom: '2px solid black',
}} {...props} />)({
  display: 'block',
  width: 170,
  color: 'white',
  backgroundColor: 'black',
  textAlign: 'center',
  border: '2px solid black',
  padding: 10,
  cursor: 'pointer',
  textDecoration: 'none',
  '&:focus, &:hover, &:visited, &:link, &:active': {
    textDecoration: 'none',
    color: 'white',
  },
})

const Button = styled.button({
  display: 'block',
  width: 170,
  color: 'black',
  backgroundColor: 'white',
  textAlign: 'center',
  border: '2px solid black',
  padding: 10,
  cursor: 'pointer',
  '&:focus, &:hover': {
    color: 'white',
    backgroundColor: 'black',
  },
})

const FutureSelect = styled.div({
  borderRadius: 5,
  backgroundColor: 'white',
  padding: '8px 45px',
})
