require('dotenv').config()
const concurrently = require('concurrently')
const ngrok = require('ngrok')

;(async function() {
  await ngrok.authtoken('2Pe1PjWLjpXWZUDEVGJpC_4HgyB9jt4Vdaw7673o5dZ')

  const frontendUrl = await ngrok.connect(3000)
  console.log('frontend url: ' + frontendUrl)
  
  const dashboardUrl = await ngrok.connect(3001)
  console.log('dashboard url: ' + dashboardUrl)

  await concurrently([
    `node devFrontend`,
    `node devDashboard`,
  ])
})().then(() => {
  console.log('finished')
}).catch(err => {
  console.log(err)
})
