const { override, addBabelPlugins } = require('customize-cra')

module.exports = override(
  ...addBabelPlugins(
    'polished',
    process.env.NODE_ENV === 'production'
      ? 'emotion'
      : ["emotion", { "sourceMap": true }],
  ),
)
