const firebase = require('firebase')

const firebaseConfig = {
  apiKey: "AIzaSyD21M93-JHfXBywAQr51hakBG3s49-dANo",
  authDomain: "protegematic.firebaseapp.com",
  databaseURL: "https://protegematic.firebaseio.com",
  projectId: "protegematic",
  storageBucket: "protegematic.appspot.com",
  messagingSenderId: "705431990403",
  appId: "1:705431990403:web:e4b276db8043dae0"
};

firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()

const bogotaPosition = {
  lat: 4.6046985,
  lng: -74.06819039999999,
}

const lideres = [
  'Lider Anonimo 1',
  'Lider Anonimo 2',
  'Lider Anonimo 3',
  'Lider Anonimo 4',
  'Lider Anonimo 5',
  'Juan Perdomo',
  'Andres Pencue',
  'Lider Anonimo 6',
  'Lider Anonimo 7',
  'Pedro Lumie',
]

const lideresStates = [
  'sos',
  'sos',
  'warn',
  'warn',
  'warn',
  'ok',
  'ok',
  'ok',
  'ok',
  'ok',
]

// const lideresStates = []
const positions = []

for (let i = 0; i < 10; i++) {
  positions.push({
    lat: bogotaPosition.lat - Math.random() * 0.1 + Math.random() * 0.1,
    lng: bogotaPosition.lng - Math.random() * 0.1 + Math.random() * 0.1,
    xDir: -Math.random() + Math.random(),
    yDir: -Math.random() + Math.random(),
  })
  // lideresStates.push(stateProb[Math.floor(Math.random() * 9.999)])
}

for (let i = 0; i < 10; i++) {
  function setNextPositions() {
    const docId = 'lider' + i
    
    positions[i].lat += Math.random() * 0.00001 * positions[i].xDir - Math.random() * 0.00001 * positions[i].yDir
    positions[i].lng += Math.random() * 0.00001 * positions[i].xDir - Math.random() * 0.00001 * positions[i].yDir

    db.collection('users').doc(docId).set({
      name: lideres[i],
      lat: positions[i].lat,
      lng: positions[i].lng,
      state: lideresStates[i],
    }).then(() => {
        console.log('Document written with ID')
        setTimeout(() => {
          setNextPositions()
        }, 1000)
      })
      .catch((error) => {
        console.error('Error adding document: ', error)
        setTimeout(() => {
          setNextPositions()  
        }, 1000)
      })
  }
  
  setNextPositions()

}
