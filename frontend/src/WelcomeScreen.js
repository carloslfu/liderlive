import React from 'react'

export default () => (
  <div style={{ 
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }}>
    <img src={require('./assets/usaid.png')} alt="USAID logo" />
    <br />
    <img src={require('./assets/ibm.png')} alt="IBM logo" />
  </div>
)
