import React from 'react';
import { Switch, Route, Redirect, HashRouter as Router } from 'react-router-dom'

import './App.css'

import Main from './Main'
import Login from './Login'
import Registro from './Registro'
import LiderEnAccion from './LiderEnAccion'

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/principal" component={Main} exact />
          <Route path="/lider-en-accion" component={LiderEnAccion}/>
          <Route path="/registro" component={Registro}/>
          <Redirect path="/principal"/>
        </Switch>
      </div>
    </Router>
  )
}

export default App
