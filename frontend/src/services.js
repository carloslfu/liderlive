import db from './db'

db.collection('users').doc('user1').update({ name: 'Luis Ortiz' }).then(() => {
  console.log('Document written with ID')
})
.catch((error) => {
  console.error('Error adding document: ', error)
})

function getLocation() {
  navigator.geolocation.getCurrentPosition(position => {
    db.collection('users').doc('user1').update({
      lat: position.coords.latitude,
      lng: position.coords.longitude,
    })
    .then(() => {
      setTimeout(() => getLocation(), 1000)
    })
    .catch((error) => {
      console.error('Error adding document: ', error);
      setTimeout(() => getLocation(), 1000)
    });
  })
}

getLocation()
