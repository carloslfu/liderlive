import React from 'react'
import { withRouter } from 'react-router-dom'
import styled from '@emotion/styled'

import WelcomeScreen from './WelcomeScreen'

export default withRouter(({ history }) => {
  const [state, setState] = React.useState('login')

  const login = () => {
    setState('welcome')
    setTimeout(() => {
      history.push('/principal')
    }, 1500)
  }

  return (
    state === 'welcome' ? <WelcomeScreen /> : <div style={{
        background: '#FFD300',
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <img src={require('./assets/logo.svg')} alt="Lider Live logo" />
      <br />
      <TextField label="Usuario" />
      <TextField label="Contraseña" type="password" />
      <div style={{ margin: 15, flexShrink: 0 }}>
        <Button onClick={login}>Ingresar</Button>
      </div>
    </div>
  )
})

const TextField = ({ label, ...rest }) => (
  <div style={{
    margin: 10,
  }}>
    {label}:
    <br />
    <input
      style={{
        padding: 10,
        fontSize: 18,
      }}
      {...rest}
    />
  </div>
)

const Button = styled.button({
  display: 'block',
  border: '2px solid black',
  padding: 10,
  color: 'black',
  backgroundColor: 'transparent',
})


// const ButtonLink = styled(Link)({
//   display: 'block',
//   border: '2px solid black',
//   padding: 10,
//   textDecoration: 'none',
//   color: 'black',
//   '&:focus, &:hover, &:visited, &:link, &:active': {
//     textDecoration: 'none',
//     color: 'black',
//   },
// })
