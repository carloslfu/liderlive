import React from 'react'
import styled from '@emotion/styled'
import { Marker, InfoWindow, Polyline } from 'google-maps-react'
import Map, { withGoogleAPI } from '../components/Map'

export default withGoogleAPI(class extends React.PureComponent {
  render() {
    const {
      markers,
      polylines,
      mapHeight,
      activeMarker,
      showingInfoWindow,
      selectedPlace,
      onMapInstance,
      onMarkerClick,
    } = this.props
    return (
      <Map
        containerStyle={{ height: mapHeight }}
        zoom={6}
        initialCenter={{ lat: 4.624335, lng: -74.063644 }}
        onClick={this.onMapClicked}
      >
        <LeakMapInstance onMapInstance={onMapInstance} />
        {markers.map((marker, idx) => (
          <Marker
            key={idx}
            title={`${marker.title} - ${getInputPlaceholder(marker.idx)}`}
            onClick={onMarkerClick}
            position={marker.position}
          />
        ))}
        {polylines.map((polyline, idx) => (
          <Polyline
            key={idx}
            path={polyline}
            strokeColor="#0000FF"
            strokeOpacity={0.8}
            strokeWeight={4}
          />
        ))}
        <InfoWindow
          marker={activeMarker}
          visible={showingInfoWindow}>
            <InfoWindowTitle>
              {selectedPlace.title}
            </InfoWindowTitle>
        </InfoWindow>
      </Map>
    )
  }
})

export const getInputPlaceholder = (idx, max) =>
  idx === 0
    ? 'Inicio'
    : idx === max - 1
    ? 'Fin'
    : `Destino ${idx}`

const InfoWindowTitle = styled.div({
  fontSize: 16,
})

class LeakMapInstance extends React.PureComponent {
  componentDidUpdate() {
    if (this.props.map) {
      this.props.onMapInstance(this.props.map)
    }
  }
  render() {
    return <></>
  }
}
