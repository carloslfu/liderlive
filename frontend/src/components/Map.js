import React, { Component } from 'react'
import { Map } from 'google-maps-react'
import withGoogleAPIBuilder from '../maps/withGoogleAPIBuilder'
import mapOptions from '../mapOptions'

export class MapContainer extends Component {

  componentDidMount() {
  }

  render() {
    return (
      <Map
        google={this.props.google}
        {...this.props}
      >
      </Map>
    );
  }
}

export const withGoogleAPI = withGoogleAPIBuilder(mapOptions)

export default withGoogleAPI(MapContainer)
